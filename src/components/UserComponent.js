import React,{useState} from 'react';
import './loginStyles.css';
import axios from 'axios';

function UserComponent(props) {
const [value, setValue] = useState([]);
const userProp=JSON.parse(sessionStorage.getItem("USER"));
console.log(userProp);
const activeVariable=makePostRequest(userProp);
console.log(activeVariable.constructor.name);
async function makePostRequest(userProp:any){
const res=await axios.post('http://localhost:9080/demo/v1/roleUsers',{
id:userProp.id,
name:userProp.name,
email:userProp.email,
role:userProp.role,

})
setValue(res.data);
return res.data;

};

return (
    <div>
      <table>
      <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
              </tr>
       </thead>
 <tbody>
        {value.map((item) => (
                        <tr key={item.id}>
                          {Object.values(item).map((val) => (
                            <td>{val}</td>
                          ))}
                        </tr>
                         ))}
        </tbody>
       </table>
    </div>
  );
}
export default UserComponent;