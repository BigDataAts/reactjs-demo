import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

const axios = require('axios');

function LoginComponent(props) {
  const email = useFormInput('');
  const password = useFormInput('');
  let navigate = useNavigate();
  // handle button click of login form
  const handleLogin = () => {

      if(email.value.length===0)
     toast('Email Is Required')
     if(password.value.length===0)
     toast('Password Is Required')
    else{
    getUser();

    }

  }

  function getUser(){
  axios.post('http://localhost:9080//demo/v1/user',{
                                             email: email.value,
                                             password: password.value
                                           })
    .then(response => {
                if(response.data){
             sessionStorage.setItem("USER",JSON.stringify(response.data));
             navigate("/users");
             }
         }).catch(function (error) {
                 // handle error
                 toast(error.response.data);
               });
  }

  return (
    <div>
      Login<br /><br />
      <div>
        Email<br />
        <input type="text" {...email} autoComplete="new-password"  />
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" {...password} autoComplete="new-password" />
      </div>
      <input type="button" value={'Login'} onClick={handleLogin} /><br />

    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default LoginComponent;